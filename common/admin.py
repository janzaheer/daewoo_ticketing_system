# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import (
    UserProfile, UserTickerRecords
)


class UserProfileAdmin(admin.ModelAdmin):
    list_display = (
        '__unicode__', 'user', 'user_type', 'password_view',
    )

    list_filter = (
        'user_type',
    )

    search_fields = (
        'user__username', 'user_type',
    )


class UserTicketRecordAdmin(admin.ModelAdmin):
    list_display = (
        '__unicode__', 'ticket_amount', 'ticket_quantity',
        'ticket_status', 'record_date'
    )
    search_fields = ('user__name',)
    raw_id_fields = ('user',)


admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(UserTickerRecords, UserTicketRecordAdmin)
