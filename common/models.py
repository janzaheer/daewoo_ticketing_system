# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.db import models
from django.utils import timezone
from django.db.models import Sum



class UserProfile(models.Model):
    USER_TYPE_MANAGER = 'Manager'
    USER_TYPE_ACCOUNTANT = 'Accountant'
    USER_TYPE_MUNSHI = 'Munshi'
    USER_TYPE_INCHARGE = 'Incharge'

    USER_TYPES = (
        (USER_TYPE_MANAGER, 'Manager'),
        (USER_TYPE_ACCOUNTANT, 'Accountant'),
        (USER_TYPE_MUNSHI, 'Munshi'),
        (USER_TYPE_INCHARGE, 'Incharge'),
    )

    user = models.OneToOneField(User, related_name='user_profile')
    user_type = models.CharField(
        max_length=100, choices=USER_TYPES, default=USER_TYPE_MANAGER
    )
    password_view = models.CharField(max_length=100, blank=True, null=True)

    def __unicode__(self):
        return self.user.username

    def munshi_records_total(self):
        if self.user_type == self.USER_TYPE_MUNSHI:
            ticket_records = UserTickerRecords.objects.filter(user=self.user)
            ticket_records = ticket_records.filter(
                record_date__day=timezone.now().day,
                record_date__month=timezone.now().month,
                record_date__year=timezone.now().year
            )
            if ticket_records.exists():
                total_amount = ticket_records.aggregate(Sum('ticket_amount'))
                total_amount = float(total_amount.get('ticket_amount__sum'))

            else:
                total_amount = 0
            return total_amount


class UserTickerRecords(models.Model):
    STATUS_RESERVED = 'Reserved Ticket'
    STATUS_CANCELLED = 'Cancelled Ticket'

    STATUSES = (
        (STATUS_RESERVED, 'Reserved Ticket'),
        (STATUS_CANCELLED, 'Cancelled Ticket'),
    )

    user = models.ForeignKey(
        User, related_name='user_records'
    )
    ticket_amount = models.DecimalField(
        max_digits=65, decimal_places=2, default=0, blank=True, null=True
    )
    ticket_quantity = models.DecimalField(
        max_digits=65, decimal_places=2, default=0, blank=True, null=True
    )
    ticket_status = models.CharField(
        max_length=100, choices=STATUSES, default=STATUS_RESERVED
    )
    record_date = models.DateTimeField(
        default=timezone.now, blank=True, null=True
    )

    def __unicode__(self):
        return self.user.username

def create_profile(sender, instance, created, **kwargs):
    if created and not UserProfile.objects.filter(user=instance):
        return UserProfile.objects.create(
            user=instance
        )


# Signals
post_save.connect(create_profile, sender=User)
