# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin

from .models import Passenger, Booking, StationReservation

class PassengerAdmin(admin.ModelAdmin):
    list_display = (
        '__unicode__', 'cnic', 'passport_no', 'immigrant_card_no', 'father_name', 'mobile_no', 'address',
    )

    search_fields = (
        'name', 'cnic', 'passport_no', 'immigrant_card_no', 'father_name', 'mobile_no', 'address',
    )

class BookingAdmin(admin.ModelAdmin):
    list_display = (
        '__unicode__', 'ticket_no', 'passenger', 'station_reservation',
        'booking_date', 'departure', 'route', 'destination', 'station',
        'departure_timing', 'seats_no', 'seats_quantity', 'total_amount',
        'discount', 'reference', 'daewoo_type', 'has_payment',
    )

    @staticmethod
    def passenger(obj):
        return obj.passenger.name

    @staticmethod
    def station_reservation(obj):
        return obj.station_reservation.name

    @staticmethod
    def route(obj):
        return obj.route.name

    @staticmethod
    def destination(obj):
        return obj.destination.name

    @staticmethod
    def station(obj):
        return obj.station.name

    @staticmethod
    def departure_timing(obj):
        return obj.departure_timing.timing

    @staticmethod
    def daewoo_type(obj):
        return obj.departure_timing.daewoo_type

    search_fields = (
        'ticket_no', 'passenger__name', 'station_reservation__name', 'booking_date', 'departure', 'route__name', 'reference',
        'has_payment', 'seats_no',
    )

    list_filter = (
        'has_payment', 'station', 'destination', 'booking_date', 'seats_no',
    )

    @staticmethod
    def station(obj):
        return obj.station.name

    @staticmethod
    def destination(obj):
        return obj.destination.name

class StationReservationAdmin(admin.ModelAdmin):
    list_display = (
        '__unicode__', 'name', 'mobile_no', 'station_name',
    )

    search_fields = (
        '__unicode__', 'name', 'mobile_no', 'station_name',
    )

    list_filter = (
        'station_name',
    )

admin.site.register(Passenger, PassengerAdmin)
admin.site.register(Booking, BookingAdmin)
admin.site.register(StationReservation, StationReservationAdmin)
