from django.conf.urls import url, include
from daewoo_booking.views import (
    CreateSchedule, SeatBooking,  BookingTicket,
    VehicleBookingDetailsForm, VehicleBookingDetails, StationReservationList, AddStationReservation, DeleteStationReservation,
    TicketCancellationFormView, BookingListView, BookingDeleteView, PassengerRecordView, PassengerRecordListView, UpdateStationReservation,
    VehicleBookingCNICDetails, VehicleBookingCNICDetailsForm,
)

from daewoo_booking.api_views import (
    CreateBookingAPIView, MakeBookingPaymentAPIView
)

urlpatterns = [
    url(
        r'^schedule/$',
        CreateSchedule.as_view(),
        name='create_schedule'
    ),
    url(
        r'^seat/destination/(?P<destination_id>\d+)/'
        r'station/(?P<station_id>\d+)/departure'
        r'/(?P<departure_id>\d+)/date/(?P<date>.+)/$',
        SeatBooking.as_view(),
        name='booking_seat'
    ),
    url(
        r'^(?P<booking_id>\d+)/ticket/$',
        BookingTicket.as_view(),
        name='booking_ticket'
    ),

    url(
        r'^vehicle/details/form/$',
        VehicleBookingDetailsForm.as_view(),
        name='vehicle_details_form'
    ),

    url(
        r'^vehicle/cnic/details/form/$',
        VehicleBookingCNICDetailsForm.as_view(),
        name='vehicle_cnic_details_form'
    ),

    url(
        r'^vehicle/destination/(?P<destination_id>\d+)/'
        r'timing/(?P<timing_id>\d+)/date/(?P<date>.+)/details/',
        VehicleBookingDetails.as_view(),
        name='vehicle_details'
    ),
    url(
        r'^vehicle/cnic/destination/(?P<destination_id>\d+)/'
        r'timing/(?P<timing_id>\d+)/date/(?P<date>.+)/details/',
        VehicleBookingCNICDetails.as_view(),
        name='vehicle_cnic_details'
    ),


    url(
        r'^seat/booking/$',
        SeatBooking.as_view(),
        name='seat_booking'
    ),

    url(
        r'^ticket/cancellation/form/$',
        TicketCancellationFormView.as_view(),
        name='ticket_cancellation_form'
    ),

    url(
        r'^destination/(?P<destination_id>\d+)/'
        r'timing/(?P<timing_id>\d+)/date/(?P<date>.+)/list/',
        BookingListView.as_view(),
        name='booking_list'
    ),

    url(
        r'^(?P<pk>\d+)/delete/$',
        BookingDeleteView.as_view(),
        name='delete_booking'
    ),

    url(
        r'^passenger/record/$',
        PassengerRecordView.as_view(),
        name='passenger_record'
    ),

    url(
        r'^passenger/(?P<code>.+)/record/$',
        PassengerRecordListView.as_view(),
        name='passenger_record_result'
    ),

    # Booking API's
    url(
        r'^create/api/$',
        CreateBookingAPIView.as_view(),
        name='create_booking_api'
    ),

    url(
        r'^reservedstations/$',
        StationReservationList.as_view(),
        name='list_reservedstations'
    ),

    url(
        r'^add/reservedstations/$',
        AddStationReservation.as_view(),
        name='add_reservedstations'
    ),

    url(
        r'^delete/reservedstations/(?P<pk>\d+)/$',
        DeleteStationReservation.as_view(),
        name='delete_reservedstations'
    ),

    url(
        r'^update/reservedstations/(?P<pk>\d+)/$',
        UpdateStationReservation.as_view(),
        name='update_reservedstations'
    ),

    url(
        r'^(?P<booking_id>\d+)/payment/api/$',
        MakeBookingPaymentAPIView.as_view(),
        name='make_payment_api'
    )
]
