# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-12-13 09:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('daewoo_booking', '0005_auto_20181213_0930'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='has_payment',
            field=models.BooleanField(default=True),
        ),
    ]
