# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import TemplateView, FormView, DeleteView, UpdateView
from django.core.urlresolvers import reverse_lazy, reverse
from django.utils import timezone
from django.http import HttpResponseRedirect

from daewoo_route.models import Destination, DepartureTiming, Stations
from daewoo_booking.models import Booking, StationReservation, Passenger
from daewoo_staff.models import Driver, Employee
from daewoo_vehicle.models import Daewoo
from common.models import UserTickerRecords
from daewoo_booking.forms import StationReservationForm


class CreateSchedule(TemplateView):
    template_name = 'booking/create_schedule.html'

    def get_context_data(self, **kwargs):
        context = super(CreateSchedule, self).get_context_data(**kwargs)
        destinations = Destination.objects.filter(
            route=self.request.user.user_route.route)
        context.update({
            'destinations': destinations
        })
        return context


class SeatBooking(TemplateView):
    template_name = 'booking/seat_booking.html'

    def get_context_data(self, **kwargs):
        context = super(SeatBooking, self).get_context_data(**kwargs)
        destination = Destination.objects.get(
            id=self.kwargs.get('destination_id'))
        departure = DepartureTiming.objects.get(
            id=self.kwargs.get('departure_id')
        )
        station = Stations.objects.get(
            id=self.kwargs.get('station_id')
        )
        reserved_date = self.kwargs.get('date')

        bookings = Booking.objects.filter(
            destination=destination,
            departure_timing=departure,
            route=destination.route,
            departure=reserved_date,
        )
        all_seats = []
        for booking in bookings:
            seats = booking.seats_no
            seats_list = [x.strip() for x in seats.split(',')]
            if seats_list:
                all_seats += seats_list

        context.update({
            'seats_list': all_seats
        })

        station_reservations = (
            StationReservation.objects.all().order_by('name'))

        context.update({
            'destination': destination,
            'departure': departure,
            'station': station,
            'reserved_date': reserved_date,
            'station_reservations':station_reservations,

        })

        return context


class BookingTicket(TemplateView):
    template_name = 'booking/booking_ticket.html'

    def get_context_data(self, **kwargs):
        context = super(BookingTicket, self).get_context_data(**kwargs)
        booking = Booking.objects.get(id=self.kwargs.get('booking_id'))

        context.update({
            'booking': booking
        })
        return context


class BookingCancelation(TemplateView):
    template_name = ''

    def get_context_data(self, **kwargs):
        context = super(
            BookingCancelation, self).get_context_data(**kwargs)


class VehicleBookingDetailsForm(TemplateView):
    template_name = 'booking/vehicle_details_form.html'

    def get_context_data(self, **kwargs):
        context = super(
            VehicleBookingDetailsForm, self).get_context_data(**kwargs)
        destinations = Destination.objects.filter(
            route=self.request.user.user_route.route)
        context.update({
            'destinations': destinations
        })
        return context


class VehicleBookingCNICDetailsForm(TemplateView):
    template_name = 'booking/vehicle_cnic_details_form.html'

    def get_context_data(self, **kwargs):
        context = super(
            VehicleBookingCNICDetailsForm, self).get_context_data(**kwargs)
        destinations = Destination.objects.filter(
            route=self.request.user.user_route.route)
        context.update({
            'destinations': destinations
        })
        return context


class VehicleBookingDetails(TemplateView):
    template_name = 'booking/vehicle_booking_details.html'

    def get_context_data(self, **kwargs):
        context = super(
            VehicleBookingDetails, self).get_context_data(**kwargs)

        driver = Driver.objects.all()
        vehicle= Daewoo.objects.all()
        timing_id = self.kwargs.get('timing_id')
        destination_id = self.kwargs.get('destination_id')
        departure_date = self.kwargs.get('date')

        departure_timing = DepartureTiming.objects.get(
            id=timing_id, destination__id=destination_id
        )
        vehicle_booking_list = Booking.objects.filter(
            departure_timing=departure_timing, departure=departure_date
        )

        all_seats = []
        for booking in vehicle_booking_list:
            seats = booking.seats_no
            seats_list = [x.strip() for x in seats.split(',')]
            if seats_list:
                all_seats += seats_list

        passengers = Passenger.objects.all()
        station_reservations = StationReservation.objects.all()

        context.update({
            'drivers' : driver,
            'vehicles': vehicle,
            'booking_list': vehicle_booking_list,
            'all_seats': all_seats,
            'departure_timing': departure_timing,
            'departure_date': departure_date,
            'station_booking_list': vehicle_booking_list.exclude(
                passenger__in=[p.id for p in passengers]
            ),
            'passenger_booking_list': vehicle_booking_list.exclude(
                station_reservation__in=[sr.id for sr in station_reservations]
            ),
        })

        return context


class VehicleBookingCNICDetails(TemplateView):
    template_name = 'booking/vehicle_booking_cnic_details.html'

    def get_context_data(self, **kwargs):
        context = super(
            VehicleBookingCNICDetails, self).get_context_data(**kwargs)

        driver = Driver.objects.all()
        vehicle = Daewoo.objects.all()
        timing_id = self.kwargs.get('timing_id')
        destination_id = self.kwargs.get('destination_id')
        departure_date = self.kwargs.get('date')

        departure_timing = DepartureTiming.objects.get(
            id=timing_id, destination__id=destination_id
        )
        vehicle_booking_list = Booking.objects.filter(
            departure_timing=departure_timing, departure=departure_date
        )

        all_seats = []
        for booking in vehicle_booking_list:
            seats = booking.seats_no
            seats_list = [x.strip() for x in seats.split(',')]
            if seats_list:
                all_seats += seats_list

        passengers = Passenger.objects.all()
        station_reservations = StationReservation.objects.all()

        context.update({
            'drivers': driver,
            'vehicles': vehicle,
            'booking_list': vehicle_booking_list,
            'all_seats': all_seats,
            'departure_timing': departure_timing,
            'departure_date': departure_date,
            'station_booking_list': vehicle_booking_list.exclude(
                passenger__in=[p.id for p in passengers]
            ),
            'passenger_booking_list': vehicle_booking_list.exclude(
                station_reservation__in=[sr.id for sr in station_reservations]
            ),
        })

        return context


class TicketCancellationFormView(TemplateView):
    template_name = 'booking/ticket_cancellation_form.html'

    def get_context_data(self, **kwargs):
        context = super(
            TicketCancellationFormView, self).get_context_data(**kwargs)
        destinations = Destination.objects.filter(
            route=self.request.user.user_route.route)
        context.update({
            'destinations': destinations
        })
        return context


class BookingListView(TemplateView):
    template_name = 'booking/booking_list.html'

    def get_context_data(self, **kwargs):
        context = super(BookingListView, self).get_context_data(**kwargs)
        timing_id = self.kwargs.get('timing_id')
        destination_id = self.kwargs.get('destination_id')
        departure_date = self.kwargs.get('date')

        departure_timing = DepartureTiming.objects.get(
            id=timing_id, destination__id=destination_id
        )
        vehicle_booking_list = Booking.objects.filter(
            departure_timing=departure_timing, departure=departure_date
        ).order_by('passenger__name')
        context.update({
            'booking_list': vehicle_booking_list,
            'departure_timing': departure_timing
        })

        return context


class BookingDeleteView(DeleteView):
    model = Booking
    success_url = reverse_lazy('booking:create_schedule')

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        # booking = Booking.objects.get(id=self.request.POST.get('pk'))
        UserTickerRecords.objects.create(
            user=self.request.user,
            ticket_amount=self.object.total_amount,
            ticket_quantity=self.object.seats_quantity,
            ticket_status=UserTickerRecords.STATUS_CANCELLED,
            record_date=timezone.now()
        )
        success_url = self.get_success_url()
        self.object.delete()
        return HttpResponseRedirect(success_url)

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class PassengerRecordView(TemplateView):
    template_name = 'booking/passenger_record.html'

    def get_context_data(self, **kwargs):
        context = super(PassengerRecordView, self).get_context_data(**kwargs)
        return context


class PassengerRecordListView(TemplateView):
    template_name = 'booking/passenger_booking.html'

    def get_context_data(self, **kwargs):
        context = super(PassengerRecordListView, self).get_context_data(**kwargs)

        bookings = Booking.objects.filter(passenger__cnic=self.kwargs.get('code'))
        if bookings.exists():
            context.update({
                'bookings': bookings
            })
            return context

        bookings = Booking.objects.filter(passenger__passport_no=self.kwargs.get('code'))
        if bookings.exists():
            context.update({
                'bookings': bookings
            })
            return context

        bookings = Booking.objects.filter(passenger__immigrant_card_no=self.kwargs.get('code'))
        if bookings.exists():
            context.update({
                'bookings': bookings
            })
            return context

        return context


class StationReservationList(TemplateView):
    template_name = 'stations/list_reservedstations.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif(
            self.request.user.user_profile.user_type ==
            self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(reverse('booking:create_schedule'))
        else:
            return super(
                StationReservationList,self).dispatch(request, *args, **kwargs)

    @staticmethod
    def get_stationresevation():
        """
        returns all stationreservations
        :return:
        """
        return StationReservation.objects.all()
    def get_context_data(self, **kwargs):
        context = super(StationReservationList, self).get_context_data(**kwargs)

        context.update({
            'stationreservations':self.get_stationresevation()
        })
        return context


class AddStationReservation(FormView):
    form_class = StationReservationForm
    template_name = 'stations/add_reservedstations.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif(
            self.request.user.user_profile.user_type ==
            self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(reverse('booking:create_schedule'))
        else:
            return super(AddStationReservation,self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AddStationReservation, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        stationreservation = form.save()
        return HttpResponseRedirect(reverse('booking:list_reservedstations'))

    def form_invalid(self, form):
        return HttpResponseRedirect(reverse('booking:add_reservedstations'))


class DeleteStationReservation(DeleteView):
    model = StationReservation
    success_url = reverse_lazy('booking:list_reservedstations')
    success_message= "Deleted Station "

    def get (self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class UpdateStationReservation(UpdateView):
    form_class = StationReservationForm
    template_name = 'stations/update_reservedstations.html'
    model = StationReservation
    success_url = reverse_lazy('booking:list_reservedstations')

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif(
            self.request.user.user_profile.user_type ==
            self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(reverse('booking:create_schedule'))
        else:
            return super(UpdateStationReservation,self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        stationreservation = form.save()
        return HttpResponseRedirect(reverse('booking:list_reservedstations'))

    def form_invalid(self, form):
        return HttpResponseRedirect(reverse('booking:add_reservedstations'))
