# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User


class Route(models.Model):
    name = models.CharField(
        max_length=200, unique=True
    )
    via = models.CharField(max_length=100, blank=True, null=True)
    to = models.CharField(max_length=100, blank=True, null=True)

    def __unicode__(self):
        return self.name


class RouteUser(models.Model):
    route = models.ForeignKey(Route, related_name='route_user')
    user = models.OneToOneField(User, related_name='user_route')

    def __unicode__(self):
        return self.user.username


class Destination(models.Model):
    name = models.CharField(
        max_length=200, unique=True
    )
    route = models.ForeignKey(
        Route, related_name='route_destination',
        null=True, blank=True,
    )

    def __unicode__(self):
        return self.name


class Stations(models.Model):
    TYPE_2X1 = '2 X 1'
    TYPE_2X2 = '2 X 2'
    TYPE_HINO = 'HINO'

    DAEWOO_TYPES = (
        (TYPE_2X1, '2 X 1'),
        (TYPE_2X2, '2 X 2'),
        (TYPE_HINO, 'HINO'),
    )

    name = models.CharField(max_length=100)
    fare = models.CharField(max_length=100)
    daewoo_type = models.CharField(
        max_length=100, choices=DAEWOO_TYPES, default=TYPE_2X1
    )
    destination = models.ForeignKey(
        Destination,
        related_name='destination_station', null=True, blank=True,
    )

    def __unicode__(self):
        return self.name


class RouteTimings(models.Model):
    station = models.ForeignKey(
        Stations,
        related_name='station_timing', null=True, blank=True
    )
    time = models.CharField(max_length=100)

    def __unicode__(self):
        return self.station.name


class DepartureTiming(models.Model):
    TYPE_2X1 = '2 X 1'
    TYPE_2X2 = '2 X 2'
    TYPE_HINO = 'HINO'

    DAEWOO_TYPES = (
        (TYPE_2X1, '2 X 1'),
        (TYPE_2X2, '2 X 2'),
        (TYPE_HINO, 'HINO'),
    )

    daewoo_type = models.CharField(
        max_length=100, choices=DAEWOO_TYPES, default=TYPE_2X1
    )

    destination = models.ForeignKey(
        Destination, related_name='destination_timing', null=True, blank=True
    )
    timing = models.CharField(max_length=100)
    vehicle = models.ForeignKey(
        'daewoo_vehicle.Daewoo', related_name='vehicle_timing',
        null=True, blank=True
    )

    def __unicode__(self):
        return self.timing
