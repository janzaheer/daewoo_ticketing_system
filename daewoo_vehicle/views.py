# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic import FormView, UpdateView, DeleteView
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse, reverse_lazy
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from daewoo_vehicle.models import Daewoo
from daewoo_vehicle.forms import DaewooForm


class VehicleList(TemplateView):
    template_name = 'daewoo/vehicle_list.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                VehicleList, self).dispatch(request, *args, **kwargs)

    @staticmethod
    def get_daewoo():
        """
        returns all daewoos
        :return:
        """
        return Daewoo.objects.all()

    def get_context_data(self, **kwargs):
        context = super(VehicleList, self).get_context_data(**kwargs)

        context.update({
            'daewoos': self.get_daewoo()
        })
        return context


class AddDaewoo(FormView):
    form_class = DaewooForm
    template_name = 'daewoo/add_daewoo.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                AddDaewoo, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AddDaewoo, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        daewoo = form.save()
        return HttpResponseRedirect(reverse('vehicle:vehicle_list'))

    def form_invalid(self, form):
        return HttpResponseRedirect(reverse('vehicle:add_daewoo'))


class VehicleDeleteView(DeleteView):
    model = Daewoo
    success_url = reverse_lazy('vehicle:vehicle_list')
    success_message = "Delete vehicle Successfully"

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class VehicleUpdateView(UpdateView):
    form_class = DaewooForm
    template_name = 'daewoo/update_vehicle.html'
    model = Daewoo
    success_url = reverse_lazy('vehicle:vehicle_list')

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                VehicleUpdateView, self).dispatch(request, *args, **kwargs)
