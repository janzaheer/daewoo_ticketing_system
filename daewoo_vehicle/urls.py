from django.conf.urls import url, include
from daewoo_vehicle.views import AddDaewoo
from daewoo_vehicle.views import VehicleList
from daewoo_vehicle.views import VehicleDeleteView
from daewoo_vehicle.views import VehicleUpdateView
urlpatterns = [
    url(
        r'^add/$',
        AddDaewoo.as_view(),
        name='add_daewoo'
    ),

    url(
        r'^list/$',
        VehicleList.as_view(),
        name='vehicle_list'
    ),

    url(
        r'^delete/(?P<pk>\d+)/$',
        VehicleDeleteView.as_view(),
        name='vehicle_delete'
    ),

    url(
        r'^update/(?P<pk>\d+)/$',
        VehicleUpdateView.as_view(),
        name='vehicle_update'
    )

]
