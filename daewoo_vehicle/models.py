# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.db.models import Sum


class Daewoo(models.Model):
    TYPE_2X1 = '2 X 1'
    TYPE_2X2 = '2 X 2'
    TYPE_HINO = 'HINO'

    DAEWOO_TYPES = (
        (TYPE_2X1, '2 X 1'),
        (TYPE_2X2, '2 X 2'),
        (TYPE_HINO, 'HINO'),
    )

    daewoo_type = models.CharField(
        max_length=100, choices=DAEWOO_TYPES, default=TYPE_2X1
    )
    plate_no = models.CharField(max_length=100,)
    name = models.CharField(max_length=100, blank=True, null=True)
    vip_seat = models.BooleanField(default='false')
    total_seats = models.IntegerField(default=0, blank=True, null=True)

    def __unicode__(self):
        return '%s (%s)' % (self.plate_no, self.name)
